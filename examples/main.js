function htmlToElement(html) {
	var template = document.createElement('template');
	template.innerHTML = html.trim();
	return template.content.firstChild;
}

function onSubmit() {
	let nameBox = document.querySelector('#name');

	var element = document.createElement('h1');
	element.innerHTML = 'Hello ' + nameBox.value;
	document.body.appendChild(element);
}

function toggleDog() {
	let dog = document.querySelector('#dogPic');
	if (dog.style.display == 'none') {
		dog.style.display = 'inline';
	} else {
		dog.style.display = 'none';
	}
}

function promptMe(){
	let color = prompt('Whats your favorite color?');
	alert(`Your favorite color is ${color}!`);
}